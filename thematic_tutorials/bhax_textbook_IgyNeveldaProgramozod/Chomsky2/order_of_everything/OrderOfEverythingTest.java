package order;

//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.equalTo;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderOfEverythingTest {

	public void orderShouldReturnExpected(Collection<Integer> input, List<Integer> expectedOutput) {
		
		List<Integer> actualOutput = createOrderedList(input);
		
		//assertThat(actualOutput, equalTo(expectedOutput));
		
	}
	
	private <T extends Comparable<T>> List<T> createOrderedList(Collection<T> input) {
		
        return input.stream().sorted().collect(Collectors.toList());
    }
	
}
