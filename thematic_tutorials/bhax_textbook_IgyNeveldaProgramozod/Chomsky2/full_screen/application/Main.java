package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Pane p = new Pane();
			Scene scene = new Scene(p,1920,1080);
			Button b = new Button();
			b.setLayoutX(500);
			b.setLayoutY(450);
			b.setText("Hello");
			p.getChildren().add(b);
			Button a = new Button();
			a.setLayoutX(500);
			a.setLayoutY(400);
			a.setText("World!");
			p.getChildren().add(a);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
