package order;

public class Main {

	public static void main(String[] args) {
		
		IntegerCollection collection = new IntegerCollection(3);
		collection.add(7);
		collection.add(3);
		collection.add(2);
		System.out.println(collection);
		collection.sort();
		System.out.println(collection);
		System.out.println(collection.contains(7));
		System.out.println(collection.contains(2));
		System.out.println(collection.contains(3));
		System.out.println(collection.contains(8));
		System.out.println(collection.contains(4));
	}

}
