package lists;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListsPerformance {

	public static void main(String[] args) {

		LinkedList<Integer> linkedlist = new LinkedList<Integer>();
		ArrayList<Integer> arraylist = new ArrayList<Integer>();
		
		//array list add
		long start = System.nanoTime();
		
		for (int i = 0; i < 10000; i++) {
			arraylist.add(i);
		}
		
		long end = System.nanoTime();
		long duration = end - start;
		System.out.println("arraylist add:" + duration);
		
		//linkedlist add
		start = System.nanoTime();
		
		for (int i = 0; i < 10000; i++) {
			linkedlist.add(i);
		}
		
		end = System.nanoTime();
		duration = end - start;
		System.out.println("linkedlist add:" + duration);
		
		//arraylist get
		start = System.nanoTime();
		
		for (int i = 0; i < 10000; i++) {
			arraylist.get(i);
		}
		
		end = System.nanoTime();
		duration = end - start;
		System.out.println("arraylist get:" + duration);
		
		//linkedlist get
		start = System.nanoTime();
		
		for (int i = 0; i < 10000; i++) {
			linkedlist.get(i);
		}
		
		end = System.nanoTime();
		duration = end - start;
		System.out.println("linkedlist get:" + duration);
		
		//arraylist remove
		start = System.nanoTime();
		
		for (int i = 9999; i > 0; i--) {
			arraylist.remove(i);
		}
		
		end = System.nanoTime();
		duration = end - start;
		System.out.println("arraylist remove:" + duration);
		
		//linkedlist remove
		start = System.nanoTime();
		
		for (int i = 9999; i > 0; i--) {
			linkedlist.remove(i);
		}
		
		end = System.nanoTime();
		duration = end - start;
		System.out.println("linkedlist remove:" + duration);
		//System.out.println(arraylist);
		//System.out.println(linkedlist);
	}
	
}
