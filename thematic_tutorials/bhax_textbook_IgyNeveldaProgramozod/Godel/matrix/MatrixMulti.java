package matrix;

import java.util.Arrays;
import java.util.stream.IntStream;

public class MatrixMulti {

	public static void main(String[] args) {
		
		double[][] m1 = { { 4, 8 }, { 0, 2 }, { 1, 6 } }; //2 x 3
	    double[][] m2 = { { 5, 2 }, { 6, 4 } }; // 2 x 2

	    if (m1[0].length != m2.length) {
	    	System.out.println("A szorzás nem lehetséges");
	    }
	    else {
	    double[][] result = Arrays.stream(m1).map(r -> 
        IntStream.range(0, m2[0].length).mapToDouble(i -> 
            IntStream.range(0, m2.length).mapToDouble(j -> r[j] * m2[j][i]).sum()
        ).toArray()).toArray(double[][]::new);

	    System.out.println(Arrays.deepToString(result));
	    
	    }
	}
	
}

	//a feltetel c1 = r2 
	//m1[0].length megadja egy soron belüli elemek számát, tehát az oszlopok szamat
	//m1.length megadja a sorok szamat