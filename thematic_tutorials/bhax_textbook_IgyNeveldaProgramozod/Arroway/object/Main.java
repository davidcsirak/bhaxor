package object;

public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {
		
		Ember a = new Ember("Adam", 20);
		Ember b = new Ember("Eva", 21);
		
		System.out.println(a.getName());
		
		a.toString();
		
		System.out.println(b.getClass());
		
		System.out.println("HashCode=" + b.hashCode());
		
		Ember c = (Ember)a.clone();
		System.out.println(c.getName());
		
		System.out.println("c egyenlő a? " + a.equals(c));
		System.out.println("c egyenlő b? " + b.equals(c));

		

		
		
	}

}
