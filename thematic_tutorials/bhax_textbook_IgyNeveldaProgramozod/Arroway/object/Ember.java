package object;

public class Ember implements Cloneable {

	private String nev;
	private int kor;
	
	public Ember (String nev, int kor) {
		this.nev = nev;
		this.kor = kor;
	}
	
	
	public void setName(String nev) {
		this.nev = nev;
	}
	
	
	public String getName() {
		return nev;
	}
	
	@Override
	public String toString() {
			
			//return "Methods [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()+ "]";
			System.out.println("Class Name = " + getClass().getSimpleName());
			return null;
	}
		

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + kor;
		result = prime * result + ((nev == null) ? 0 : nev.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ember other = (Ember) obj;
		if (kor != other.kor)
			return false;
		if (nev == null) {
			if (other.nev != null)
				return false;
		} else if (!nev.equals(other.nev))
			return false;
		return true;
	}

	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		
		return super.clone();
	}


	/*@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}*/
	
		
		
	}


