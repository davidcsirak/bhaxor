package neptun;

import java.util.HashSet;
import java.util.Set;

public class Student {

	public Student(String name) {
		
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	private String name;
    private Set<Subject> subjects = new HashSet<Subject>();

    
    public String getName() {
        return name;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
    }

    public void removeSubject(Subject subject) {
        this.subjects.remove(subject);
    }
}
