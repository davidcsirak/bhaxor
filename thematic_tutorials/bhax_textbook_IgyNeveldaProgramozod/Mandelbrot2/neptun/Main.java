package neptun;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		
		Subject prog1 = new Subject(1,"programozás",SubjectType.LABORATORY);
		Subject webtech = new Subject(2,"webtechnologia",SubjectType.PRACTICAL);
		Lecturer batfai = new Lecturer("Bátfai Norbert");
		Lecturer jeszy = new Lecturer("Jeszenszky Péter");
		Student david = new Student("Csirák Dávid");
		Student mark = new Student("Gyöngyösi Márk");
			
		prog1.addNewStudent(david);
		prog1.addNewStudent(mark);
		prog1.addNewLecturer(batfai);
		prog1.addNewLecturer(null);
		
		webtech.addNewLecturer(jeszy);
		
		System.out.println(prog1.getStudents());
		prog1.kiirStudents();
		
		System.out.println(prog1.getLecturers());
		prog1.kiirLecturers();
		
		david.addSubject(webtech);
		System.out.println(david.getSubjects());
		david.removeSubject(prog1);
		
		System.out.println(prog1.getName());
		System.out.println(prog1.getSubjectType());
		
		System.out.println(batfai.getName());
		System.out.println(david.getName());

	}

}
