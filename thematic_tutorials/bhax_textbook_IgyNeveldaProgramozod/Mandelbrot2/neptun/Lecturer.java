package neptun;

public class Lecturer {

	@Override
	public String toString() {
		return name;
	}

	public Lecturer(String name) {
		
		this.name = name;
	}

	private String name;

    public String getName() {
        return name;
    }
}
