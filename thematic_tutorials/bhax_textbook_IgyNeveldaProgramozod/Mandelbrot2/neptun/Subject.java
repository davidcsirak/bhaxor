package neptun;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;



public class Subject {

	private int code;
    private String name;
    private SubjectType subjectType;
    public  Set<Student> students = new HashSet<Student>();
    @Override
	public String toString() {
		return name;
	}

	private Set<Lecturer> lecturers = new HashSet<Lecturer>();
    
    public Subject(int code, String name, SubjectType subjectType) {

		this.code = code;
		this.name = name;
		this.subjectType = subjectType;
		
	}

    public void kiirStudents() {
    	
    	Iterator iterator =  students.iterator();
    	
    	while (iterator.hasNext()) {
    		
    		System.out.println(iterator.next());    	
    	}
    }
    
    public void kiirLecturers() {
    	
    	Iterator iterator =  lecturers.iterator();
    	
    	while (iterator.hasNext()) {
    		
    		System.out.println(iterator.next());    	
    	}
    }
    

    
	public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public Set<Lecturer> getLecturers() {
        return lecturers;
    }

    public void addNewStudent(Student student) {
    	
        this.students.add(student);
    }

    public void addNewLecturer(Lecturer lecturer) {
        this.lecturers.add(lecturer);
    }

    
}