package character_abstract;

public abstract class Champion {

	public void startingLevel() {
		// TODO - implement Champion.startingLevel
		throw new UnsupportedOperationException();
	}

	public abstract void lane();

	public abstract void role();

}