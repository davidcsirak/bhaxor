public class ExorTitkosító {
    
    public ExorTitkosító(String kulcsSzöveg,
            java.io.InputStream bejövőCsatorna,
            java.io.OutputStream kimenőCsatorna) 
            throws java.io.IOException {        //io (kivétel)probléma esetén hibaüzenet
        
        byte [] kulcs = kulcsSzöveg.getBytes();  //byte tipusu tömb inic., byteonként feldarabolja a (string)szöveget és beteszi a tömb-be.
        byte [] buffer = new byte[256];         //byte tipusu tömb inic. aminek 256 poziciója van
        int kulcsIndex = 0;
        int olvasottBájtok = 0;

        while((olvasottBájtok =
                bejövőCsatorna.read(buffer)) != -1) {
            
            for(int i=0; i<olvasottBájtok; ++i) {
                
                buffer[i] = (byte)(buffer[i] ^ kulcs[kulcsIndex]);
                kulcsIndex = (kulcsIndex+1) % kulcs.length;
                
            }
            
            kimenőCsatorna.write(buffer, 0, olvasottBájtok);
            
        }
        
    }
    
    public static void main(String[] args) {                        //a tiszta szöveg amit megadunk a terminálba
        
        try {
            
            new ExorTitkosító(args[0], System.in, System.out);      //beolvasás
            
        } catch(java.io.IOException e) {                            //kivétel kezelés
            
            e.printStackTrace();                                    //hibaüzenet
            
        }
        
    }
    
}
