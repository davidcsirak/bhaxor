#include <stdio.h>

int
main ()
{

    int a;
    printf("egész\n");
    
    int *b = &a;
    printf("egészre mutató mutató\n");
     
    //int &r = a;
    printf("egész referenciája\n");
    
    int c[5];
    printf("gészek tömbje\n");
    
    //int (&tr)[5] = c;
    printf("egészek tömbjének referenciája (nem az első elemé)\n");  
    
    int *d[5];
    printf("egészre mutató mutatók tömbje\n"); 
    
    int *h ();
    printf("egészre mutató mutatót visszaadó függvény\n"); 
    
    int *(*l) ();
    printf("egészre mutató mutatót visszaadó függvényre mutató mutató\n");
    
    int (*v (int c)) (int a, int b);
    printf("egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény\n");
    
    int (*(*z) (int)) (int, int);
    printf("függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre\n"); 
    

    return 0;
}
