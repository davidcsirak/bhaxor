#include <iostream>
using namespace std;

class Szulo {
	
    public:
        void szulo_fv() {
	    cout << "Felnott" << endl;
	    }
	
};

class Gyerek : public Szulo {
	
    public:
        void gyerek_fv() {
	    cout << "Gyerek" << endl;
	    }
};

int main () {

    Szulo szulo; 
	Gyerek gyerek; 
		
			
	szulo.szulo_fv();
	gyerek.gyerek_fv();
	gyerek.szulo_fv();
    //szulo.gyerek_fv();  

    return 0;
}