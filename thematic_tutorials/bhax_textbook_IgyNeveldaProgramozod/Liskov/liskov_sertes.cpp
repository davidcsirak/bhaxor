#include <iostream>
using namespace std;

class Madar {
    public:

    virtual void repul() {
		
		cout << "Repül!" << endl;
	}
};

class Sas : public Madar {
public:

    void repul() override {

        cout << "Sas: Repül!" << endl;

    }
};

class Pingvin : public Madar {

};

static void madarRepul(Madar&  a) {

    a.repul();
};

int main() {

    Sas sas;
	Pingvin pingvin;
		
	madarRepul(sas);
	madarRepul(pingvin);

    return 0;
}