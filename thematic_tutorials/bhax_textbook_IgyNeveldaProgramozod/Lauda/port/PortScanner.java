package port;

import java.net.Socket;
import java.io.IOException;

public class PortScanner {

	public static void main(String[] args) {
		
		Socket socket = null;
		
		for(int i=0; i<1024; ++i) {
    
			try {
				socket = new Socket("host", i);
    
				System.out.println(i + "figyelem alatt van");
    
				socket.close();
			} catch (IOException e) {
				System.out.println(i + "nincs figyelem alatt");
			}
		}
    }
    
}