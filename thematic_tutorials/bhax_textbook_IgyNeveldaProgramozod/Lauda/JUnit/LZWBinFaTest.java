package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LZWBinFaTest {

	private LZWBinFa testSubject = new LZWBinFa(); 
	
	
	
	@Test
	void getmelysegShouldEqualtoTest() {
		
		for (char t : "01111001001001000111".toCharArray()) {
			
			testSubject.egyBitFeldolg(t);
			
		}
		
		assertEquals(4, testSubject.getMelyseg());
	}
	
	@Test
	void getszorasShouldEqualtoTest() {
		
		for (char t : "11101100110110001111010101".toCharArray()) {
			
			testSubject.egyBitFeldolg(t);
			
		}
		
		assertEquals(0.8366600265340756, testSubject.getSzoras());
	}

}
