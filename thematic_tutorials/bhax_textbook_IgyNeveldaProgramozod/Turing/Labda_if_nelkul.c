#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();  //inicializalja a curses strukturakat, refresheli a screent

    int x = 0;
    int y = 0;

    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;

    for ( ;; ) {

        getmaxyx ( ablak, my , mx );  //tarolja az ablak hatarait(meretet)

        mvprintw ( y, x, "O" );   //megadja melyik sor-oszlop-ba irja ki

        refresh ();
        usleep ( 1000 );  //masodperc milliomod reszere fuggeszti fel

        //clear();
        
        x = x + xnov;
        y = y + ynov;

        for (int k = x; k>=mx-1; k--) { // elerte-e a jobb oldalt?
            xnov = xnov * -1;
        }
        for (int k = x; k<=0; k++) { // elerte-e a bal oldalt?
            xnov = xnov * -1;
        }
        for (int k = y; k<=0; k++) { // elerte-e a tetejet?
            ynov = ynov * -1;
        }
        for (int k = y; k>=my-1; k--) { // elerte-e a aljat?
            ynov = ynov * -1;
        }

    }

    return 0;
}
