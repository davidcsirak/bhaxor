package file_task;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Finalize {

	public static void main(String[] args) throws IOException {
		
		BugousStuffProducer producer = new BugousStuffProducer("output.txt");
		producer.writeStuff();
		
	}

	private static class BugousStuffProducer {
		private final Writer writer;

		public BugousStuffProducer(String outputFileName) throws IOException {
            writer = new FileWriter(outputFileName);
        	}

		public void writeStuff() throws IOException {
            writer.write("Stuff");
            
        	}

		@Override
		public void finalize() throws IOException {
            writer.close();
        	}
    	}
}
