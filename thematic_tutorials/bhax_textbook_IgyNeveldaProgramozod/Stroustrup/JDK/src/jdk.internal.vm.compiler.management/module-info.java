/*
 * Copyright (c) 2017, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

/**
 * Registers Graal Compiler specific management interfaces for the JVM.
 *
 * @moduleGraph
 * @since 10
 */
module jdk.internal.vm.compiler.management {
    // source file: file:///scratch/opt/mach5/mesos/work_dir/slaves/dd0bab7c-a530-4905-bb6b-daa7153eb2c1-S39/frameworks/1735e8a2-a1db-478c-8104-60c8b0af87dd-0196/executors/0ce275e9-e1f6-4023-9763-930ab774e026/runs/d3617254-54a0-4797-8710-307f3b449bdf/workspace/open/src/jdk.internal.vm.compiler.management/share/classes/module-info.java
    //              file:///scratch/opt/mach5/mesos/work_dir/slaves/dd0bab7c-a530-4905-bb6b-daa7153eb2c1-S39/frameworks/1735e8a2-a1db-478c-8104-60c8b0af87dd-0196/executors/0ce275e9-e1f6-4023-9763-930ab774e026/runs/d3617254-54a0-4797-8710-307f3b449bdf/workspace/build/linux-x64-open/support/gensrc/jdk.internal.vm.compiler.management/module-info.java.extra
    requires java.management;
    requires jdk.management;
    requires jdk.internal.vm.ci;
    requires jdk.internal.vm.compiler;

    provides org.graalvm.compiler.hotspot.HotSpotGraalManagementRegistration with org.graalvm.compiler.hotspot.management.HotSpotGraalManagement;
    provides org.graalvm.compiler.serviceprovider.JMXService with org.graalvm.compiler.hotspot.management.JMXServiceProvider;
}
