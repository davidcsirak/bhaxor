#include <iostream>
#include <string>
#include <boost/filesystem.hpp>

using namespace boost::filesystem;

class Tracker
{
private:
    unsigned int numOfClasses;
public:
    Tracker() : numOfClasses(0)
    {
        
    }
   
    unsigned int getNumOfClasses()
    {
        return numOfClasses;
    }
    void listJDK(path thePath)
    {
        
        
       if (is_regular_file(thePath))
        {
                std::string ext (".java");
                if (!ext.compare(extension(thePath)))
                {
                    std::cout << thePath << std::endl;
                }
                numOfClasses++;
        }
        else if(is_directory(thePath))
                for (directory_entry& entry : directory_iterator(thePath))
                {
                        listJDK(entry.path());
                }
                
    }
};

int main()
{
    Tracker theTracker;
    std::cout << "Searching for Java classes\n";

    theTracker.listJDK("src");

    std::cout << "Found " << theTracker.getNumOfClasses() << " classes.\n";

    return 0;


}            
